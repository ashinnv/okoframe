package okoframe

import (
	"bytes"
	"encoding/json"
	"fmt"

	"gocv.io/x/gocv"
	"gopkg.in/yaml.v2"
)

type Motion struct {

	//x/y of top left and bottom right corners of rectangle over motion
	X1 int
	Y1 int
	X2 int
	Y2 int

	MotionAmount float64 //In a given set of 1000 pixels, how many would change value by more than 50%
}

type Detection struct {
	Title   string //What is being detected here
	Version string //Version of the detection model
	X1      int
	Y1      int
	X2      int
	Y2      int

	Certainty float64 //How sure are we that this is that thing
}

// Metadata for a frame
type FrameMeta struct {
	FrameId  []byte
	CapTime  uint64
	Tags     []string
	Width    int
	Height   int
	OCVType  gocv.MatType
	Channels uint8
}

type Frame struct {
	Meta     FrameMeta
	ByteData []byte
}

// Does this frame have an image in it, or is it just metadata?
func (fr Frame) IsPopulated() bool {
	//NOTE: Future versions should check better
	if len(fr.ByteData) > 0 {
		return true
	} else {
		return false
	}
}

// Generate json of frame's metadata
func (fr Frame) DumpMeta() string {

	var stash bytes.Buffer
	mtDat := fr.Meta
	encErr := json.NewEncoder(&stash).Encode(mtDat)
	if encErr != nil {
		fmt.Println("FAILED TO ENDODE FRAME META")
	}

	return stash.String()
}

// Generate json of frame's metadata
func (fr Frame) DumpMetaYaml() string {

	mtDat := fr.Meta
	yaDat, encErr := yaml.Marshal(&mtDat)
	if encErr != nil {
		fmt.Println("FAILED TO ENDODE FRAME META")
	}

	return yaDat
}

// Some number of Frames in a collection. Contains motion detections and object identifications.
type Clip struct {
	Frames     [][]byte
	Meta       FrameMeta
	Motion     []Motion
	Detections []Detection
}
