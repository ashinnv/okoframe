module okoframe

go 1.20

require (
	gocv.io/x/gocv v0.35.0
	gopkg.in/yaml.v2 v2.4.0
)
